<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'App\Http\Controllers\HomeController');

Route::get('/tableManagement', function () {
    return view('tableManagement');
});
Route::get('sentences/sentencesData', 'App\Http\Controllers\SentencesController@sentencesData')->name('sentences.sentencesData');
Route::get('/sentences/{id}/delete', 	'App\Http\Controllers\SentencesController@destroy');
Route::post('/sentences/{id}/update', 			'App\Http\Controllers\SentencesController@update');
Route::post('/sentences/fetchElement', 'App\Http\Controllers\SentencesController@fetchElement')->name('sentences.fetchElement');
Route::resource('sentences', 'App\Http\Controllers\SentencesController');
