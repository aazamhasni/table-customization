<!doctype html>
<html>
<head>
   @include('layouts.inc.head')
   @stack('styles')

</head>
<body class="pace-done">
<div id="wrapper">
    @include('layouts.inc.sidebar')
    <div id="page-wrapper" class="gray-bg" style="min-height: 969px;">
        @include('layouts.inc.navbar')
        <div class="wrapper wrapper-content animated fadeInRight">
            @yield('content')
        </div>
        <div class="footer" style="position:fixed;bottom:0px;">
            @include('layouts.inc.footer')
        </div>
    </div>
</div>
@include('layouts.inc.footer-scripts')
@stack('scripts')
</body>
</html>
