@extends('layouts.app')

@section('content')
{{-- breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
      <h2>Table Management</h2>
      <ol class="breadcrumb">
          <li>
              <a href="/">Home</a>
          </li>
          <li class="active">
              <strong>Table Management</strong>
          </li>
      </ol>
  </div>
  <div class="col-lg-2">
  </div>
</div>
</div>
{{-- content --}}
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h3 class="text-navy" >Create New Element</h3>
      </div>
      <div class="ibox-content text-center p-md">
        <form action="{{route('sentences.store')}}" method="post" class="form-horizontal">
          @csrf
          <div class="form-group">
            <button class="btn btn-primary dim pull-right" type="button" data-toggle="modal" data-target="#tableIndicator"><i class="fa fa-info-circle"></i></button>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label">* Text</label>
            <div class="col-sm-10"><input type="text" id="text" name="text" class="form-control" placeholder="Enter any text"></div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label">* Row</label>
            <div class="col-sm-10"><input type="text" id="row" name="row" class="form-control" placeholder="Enter no of row (number must be greater than 0, less than 4)"></div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label">* Column</label>
            <div class="col-sm-10"><input type="text" id="column" name="column" class="form-control" placeholder="Enter no of column (number must be greater than 0, less than 4)"></div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label">Color</label>
            <div class="col-sm-10"><input type="color" id="color" name="color" class="form-control" placeholder="Enter color (eg. red or yellow)"></div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label">Font Styling</label>
            <div class="col-sm-2">Font-Size</div>
            <div class="col-sm-8"><input type="text" id="font_size" name="font_size" class="form-control" placeholder="Enter font-size (eg. 10,20 or 30)"></div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label"></label>
            <div class="col-sm-2">Font-Weight</div>
            <div class="col-sm-8">
              <table style="width:20%">
                <tr>
                  <td >
                    <input class="pull-left" type="radio" value="normal" name="font_weight">
                  </td>
                  <td style="text-align: left">
                    Normal
                  </td>
                </tr>
                <tr>
                  <td>
                    <input class="pull-left" type="radio" value="bold" name="font_weight"> 
                  </td>
                  <td style="text-align: left">
                    Bold
                  </td>
                </tr>
              </table>
              {{-- <input type="text" id="font-weight" name="font-weight" class="form-control" placeholder="Enter font weight (normal or bold)"> --}}
            </div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label"></label>
            <div class="col-sm-2">Font-Style</div>
            <div class="col-sm-8">
              <table style="width:20%">
                <tr>
                  <td >
                    <input class="pull-left" type="radio" value="normal" name="font_style">
                  </td>
                  <td style="text-align: left">
                    Normal
                  </td>
                </tr>
                <tr>
                  <td>
                    <input class="pull-left" type="radio" value="italic" name="font_style"> 
                  </td>
                  <td style="text-align: left">
                    Italic
                  </td>
                </tr>
                <tr>
                  <td>
                    <input class="pull-left" type="radio" value="oblique" name="font_style"> 
                  </td>
                  <td style="text-align: left">
                    Oblique
                  </td>
                </tr>
              </table>
            </div>
            </div>
          <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>&nbsp;&nbsp;Save</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h3 class="text-navy" >List of Element</h3>
      </div>
      <div class="ibox-content text-center p-md">
        <table class="table table-striped table-bordered table-hover" id="element-table" >
          <thead>
              <tr>
                  <th>Name</th>
                  <th>Row</th>
                  <th>Column</th>
                  <th>Color</th>
                  <th>Style</th>
                  <th style='width:10%'></th>
              </tr>
          </thead>
      </table>
      </div>
    </div>
  </div>
</div>
{{--  edit element modal  --}}
<div class="modal" id="editElement" style="z-index: 2045 !important;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content animated bounceInRight">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                      class="sr-only">Close</span></button>
              <h3 class="modal-title text-navy" style="text-align:center">Edit Element</h3>
          </div>
          <div class="modal-body" id="editElementBody">
            
          </div>
      </div>
  </div>
</div>
{{--  table indicator modal  --}}
<div class="modal" id="tableIndicator" style="z-index: 2045 !important;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content animated bounceInRight">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                      class="sr-only">Close</span></button>
              <h3 class="modal-title text-navy" style="text-align:center">Table Indicator</h3>
              <p style="text-align:center;">ROWxCOLUMN</p>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                      <div class="ibox-content">
                        <table class="table table-bordered">
                          <tbody>
                          <tr>
                              <td>1x1</td>
                              <td>1x2</td>
                              <td>1x3</td>
                              <td>1x4</td>
                          </tr>
                          <tr>
                              <td>2x1</td>
                              <td>2x2</td>
                              <td>2x3</td>
                              <td>2x4</td>
                          </tr>
                          <tr>
                              <td>3x1</td>
                              <td>3x2</td>
                              <td>3x3</td>
                              <td>3x4</td>
                          </tr>
                          <tr>
                              <td>4x1</td>
                              <td>4x2</td>
                              <td>4x3</td>
                              <td>4x4</td>
                          </tr>
                          </tbody>
                      </table>
                      </div>
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
  $('#editElement').on('show.bs.modal', function(e) {
    $('#editElementBody').html('');
      var myDataId= $(e.relatedTarget).attr('data-id'); 
      fetchElement(myDataId)

  });

  function fetchElement(id){
    $.ajax({
          url:"{{ route('sentences.fetchElement') }}",
          method:"POST",
          data:{
              id
          },
          headers: {
              'X-CSRF-TOKEN': "{{ csrf_token() }}"
          },
          success: (resultsJSON) =>{
            let results = JSON.parse(resultsJSON);
            let style = JSON.parse(results.data.style);
            $('#editElementBody').append(`
              <div class="row">
                <div class="col-lg-12">
                  <form action="/sentences/${results.data.id}/update" method="post" class="form-horizontal">
                  @csrf
                    <input type="hidden" name="id" id="${results.data.id}" value="${results.data.id}">
                    <div class="form-group"><label class="col-sm-2 control-label">* Text</label>
                      <div class="col-sm-10"><input type="text" id="text" name="text" class="form-control" value="${results.data.text}" placeholder="Enter any text"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">* Row</label>
                      <div class="col-sm-10"><input type="text" id="row" name="row" class="form-control" value="${results.data.row}" placeholder="Enter no of row (number must be greater than 0, less than 4)"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">* Column</label>
                      <div class="col-sm-10"><input type="text" id="column" name="column" class="form-control" value="${results.data.column}" placeholder="Enter no of column (number must be greater than 0, less than 4)"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Color</label>
                      <div class="col-sm-10"><input type="color" id="color" name="color" class="form-control" value="${results.data.color ? results.data.color : ''}" placeholder="Enter color (eg. red or yellow)"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Font Styling</label>
                    <div class="col-sm-2">Font-Size</div>
                    <div class="col-sm-8"><input type="text" id="font_size" name="font_size" class="form-control" value="${style.font_size ? style.font_size : ''}" placeholder="Enter font-size (eg. 10,20 or 30)"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label"></label>
                    <div class="col-sm-2">Font-Weight</div>
                    <div class="col-sm-8">
                      <table style="width:20%">
                        <tr>
                          <td >
                            <input class="pull-left" type="radio" value="normal" name="font_weight" ${style.font_weight == 'normal' ? 'checked' : ''}>
                          </td>
                          <td style="text-align: left">
                            Normal
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <input class="pull-left" type="radio" value="bold" name="font_weight" ${style.font_weight == 'bold' ? 'checked' : ''}> 
                          </td>
                          <td style="text-align: left">
                            Bold
                          </td>
                        </tr>
                      </table>
                      </div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label"></label>
                    <div class="col-sm-2">Font-Style</div>
                    <div class="col-sm-8">
                      <table style="width:20%">
                        <tr>
                          <td >
                            <input class="pull-left" type="radio" value="normal" name="font_style" ${style.font_style == 'normal' ? 'checked' : ''}>
                          </td>
                          <td style="text-align: left">
                            Normal
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <input class="pull-left" type="radio" value="italic" name="font_style" ${style.font_style == 'italic' ? 'checked' : ''}> 
                          </td>
                          <td style="text-align: left">
                            Italic
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <input class="pull-left" type="radio" value="oblique" name="font_style" ${style.font_style == 'oblique' ? 'checked' : ''}> 
                          </td>
                          <td style="text-align: left">
                            Oblique
                          </td>
                        </tr>
                      </table>
                    </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right"><i class="fas fa-save"></i>&nbsp;&nbsp;Save</button>
                </form>
                  
                </div>
              </div>
            `)
          

          },
          error: err => console.error(err)
        })
  }
</script>
<script>
  $(document).ready( function () {
      $('#element-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{!! route('sentences.sentencesData') !!}',
          columns: [
              { data: 'text', name: 'text' },
              { data: 'row', name: 'row' },
              { data: 'column', name: 'column' },
              { data: 'color', name: 'color' },
              { data: 'style', name: 'style' },
              { data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
      
      $(document).on("click", ".data-delete", function (e) {
  var form = $(this).data('form');
  var href = $(this).attr("href");
  confirm('Are you sure you want to delete this?', function(result) {
    if(result) {
      e.preventDefault();
      if($('#form-delete-' + form).length > 0)
        $('#form-delete-' + form).submit();
      else
        window.location.href = href;
    }
  });
  return false;
      });
  });
</script>
@endpush