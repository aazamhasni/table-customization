@extends('layouts.app')
@section('content')

{{-- breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
      <h2>Home</h2>
      <ol class="breadcrumb">
          <li>
              <a href="/">Home</a>
          </li>
      </ol>
  </div>
  <div class="col-lg-2">
  </div>
</div>
{{-- content --}}
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h3 class="text-navy" >Table</h3>
      </div>
      <div class="ibox-content text-center p-md">
        <table class="table table-bordered">
          <tbody>
            @for ($i = 1; $i <= 4; $i++)
            <tr style="width=25%">
              @for($j = 1; $j <= 4; $j++)
                <td>
                  @foreach($data as $value)
                  @php
                      $style = json_decode($value->style);
                  @endphp
                    @if($value->row == $i)
                      @if($value->column == $j)
                        <span style="color:{{$value->color ? $value->color : black}}; font-size :{{$style->font_size}}px;font-weight :{{$style->font_weight}};font-style :{{$style->font_style}}">
                          {{$value->text}}
                        </span>
                      @endif
                    @endif
                  @endforeach
                </td>
              @endfor
            <tr>
            @endfor
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
@endsection