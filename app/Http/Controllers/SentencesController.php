<?php

namespace App\Http\Controllers;

use DataTables;
use Ramsey\Uuid\Uuid;
use Faker\Provider\Base;

use App\Models\Sentences;
use Illuminate\Http\Request;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\Validator;

class SentencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'text' => 'required|max:255',
            'row' => 'required|numeric|min:0|max:4',
            'column' => 'required|numeric|min:0|max:4',
        ]);
        if ($validator->fails()) {
            return redirect('tableManagement')->with('error','Data Fail to Save');
        }

        $new = new Sentences;
        $new->id = Uuid::uuid4()->getHex();
        $new->text = $request->text;
        $new->row = $request->row;
        $new->column = $request->column;
        $new->color = $request->color;

        $obj = (Object)[
            'font_size' => $request->font_size,
            'font_weight'=> $request->font_weight, 
            'font_style'=> $request->font_style,
        ];
        $style = json_encode($obj);
        $new->style = $style;
        $new->id_status = "1";
        $new->save();

        return redirect('tableManagement')->with('message','Data Successfully Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'text' => 'required|max:255',
            'row' => 'required|numeric|min:0|max:4',
            'column' => 'required|numeric|min:0|max:4',
        ]);
        if ($validator->fails()) {
            return redirect('tableManagement')->with('error','Data Fail to Save');
        }

        $update = Sentences::find($id);
        $update->text = $request->text;
        $update->row = $request->row;
        $update->column = $request->column;
        $update->color = $request->color;
        $obj = (Object)[
            'font_size' => $request->font_size,
            'font_weight'=> $request->font_weight, 
            'font_style'=> $request->font_style,
        ];
        $style = json_encode($obj);
        $update->style = $style;
        $update->save();

        return redirect('tableManagement')->with('message','Data Successfully Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove = Sentences::find($id);
        $remove->delete();    
        return redirect('tableManagement')->with('message','Data Successfully Delete');
    }

    public function sentencesData(){
        $data = Sentences::all();
        // dd($data);
        $dt = Datatables::of($data)
        ->editColumn('style', function($data) {
            $style = json_decode($data->style);
            $html = '';
            $html .= '
                Font-Size :'.$style->font_size.'
                Font-Weight :'.$style->font_weight.'
                Font-Style :'.$style->font_style.'
            ';
            return $html;
            // return '<a data-toggle="modal" data-target="#editElement" data-id='.$data->id.' class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="/sentences/'.$data->id . '/delete" class="delete btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></a>';
        })
        ->addColumn('action', function($data) {
            return '<a data-toggle="modal" data-target="#editElement" data-id='.$data->id.' class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="/sentences/'.$data->id . '/delete" class="delete btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></a>';
        })
        ->make(true);

        return $dt;
    }

    public function fetchElement(Request $request){
        $element = Sentences::find($request->id);
        $data = [
            'status' => 'success', 
            'message' => 'Successfully get element.',
            'data' => $element
        ];
        return json_encode($data);

    }
}
